# HTML内置语言服务

**Notice:** 安装这个扩展需要首先禁用VSCode内置的HTML Language Features

## 功能特征
1. 能够识别&lt;Script Src=...&gt;引用的js脚本。
2. 如果在引用的js相同的文件夹下有类型定义文件(.d.ts),扩展将使用该文件提供智能提示内容。

这个扩展是基于VSCode内置扩展进行的开发，具体功能可参照 [HTML in Visual Studio Code](https://code.visualstudio.com/docs/languages/html) .

另外，这个扩展实现了将HTML内以"@()@"为标记的内容，识别为JavaScript代码。
